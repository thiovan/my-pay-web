<?php

namespace App\Http\Controllers;

use App\ClusterPopulation;
use App\Http\Controllers\Controller;
use App\Transaction;
use App\WebArticle;
use App\WebContent;

class WebController extends Controller
{
    public function charity($slug)
    {
        return view('redirect')->with('slug', $slug)->with('method', 'charity');
    }
    public function comingsoon()
    {
        return view('comingsoon');
    }
    public function privacyPolicy()
    {
        return view('privacy-policy');
    }
    public function jimpitan($trxId)
    {
        $findStatusTrx = Transaction::where('id', $trxId)->first();
        if ($findStatusTrx->status_id == 2) {
            return view('loading')->with('slug', $trxId)->with('method', 'jimpitan');
        }

        return view('alert')->with('data', [
            'message' => 'Transaction failed, User Already Paid',
        ]);
    }
    public function confirmCluster($cluster_id, $user_id)
    {
        if (!isset($cluster_id) || !isset($user_id)) {
            return view('alert')->with('data', [
                'message' => 'The given data was invalid',
            ]);
        }

        $findPopulation = ClusterPopulation::where('user_id', $user_id)->first();
        if ($findPopulation == null) {
            return view('alert')->with('data', [
                'message' => 'User not registered to any cluster',
            ]);
        }

        if ($findPopulation->approve == 1) {
            return view('alert')->with('data', [
                'message' => 'User already approved',
            ]);
        }

        $findPopulation->approve = 1;
        if ($findPopulation->save()) {
            return view('loading')->with('slug', $cluster_id)->with('method', 'jimpitanapprove');
        }

        return view('alert')->with('data', [
            'message' => 'User approval failed',
        ]);
    }
    public function declineCluster($cluster_id, $user_id)
    {
        if (!isset($cluster_id) || !isset($user_id)) {
            return view('alert')->with('data', [
                'message' => 'The given data was invalid',
            ]);
        }

        $findPopulation = ClusterPopulation::where('user_id', $user_id)->first();
        if ($findPopulation == null) {
            return view('alert')->with('data', [
                'message' => 'User not registered to any cluster',
            ]);
        }

        if ($findPopulation->approve == 1) {
            return view('alert')->with('data', [
                'message' => 'User already approved',
            ]);
        }

        if ($findPopulation->delete()) {
            return view('loading')->with('slug', $cluster_id)->with('method', 'jimpitandecline');
        }

        return view('alert')->with('data', [
            'message' => 'Decline cluster failed',
        ]);
    }
    public function landingPage()
    {
        $contents = WebContent::all();
        $value    = [];
        foreach ($contents as $content) {
            $value[$content->key] = json_decode($content->value);
        }
        $article = WebArticle::whereNotIn('tag', ['promo'])->orderBy('created_at', 'desc')->take(5)->get();
        $promo   = WebArticle::where('tag', 'promo')->orderBy('created_at', 'desc')->take(5)->get();
        return view('landing-page')->with('value', (object) $value)->with('articles', $article)->with('promos', $promo);
    }
    public function articlePage($article_id)
    {
        $contents = WebContent::all();
        $value    = [];
        foreach ($contents as $content) {
            $value[$content->key] = json_decode($content->value);
        }
        $article = WebArticle::find($article_id);
        return view('article-page')->with('value', (object) $value)->with('article', $article);
    }
}
