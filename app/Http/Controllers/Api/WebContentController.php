<?php

namespace App\Http\Controllers\Api;

use App\WebArticle;
use App\WebContent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class WebContentController extends Controller
{
    public function getContentLanding()
    {
        $contents = WebContent::all();
        $value = [];
        foreach ($contents as $content) {
            $value[$content->key] = json_decode($content->value);
        }
        $content = json_encode($value);

        return response()->json($content);
    }

    public function postContentLanding(Request $request)
    {

        if ($request->hasFile('service_icon')) {
            $imageFile = $request->file('service_icon');
            for ($i = 0; $i < count($imageFile); $i++) {
                $imageName = $imageFile[$i]->getClientOriginalName() . '.' . $imageFile[$i]->getClientOriginalExtension();
                $destinationPath = public_path('/appy/images');
                $imageFile[$i]->move($destinationPath, $imageName);
            }
        }

        $postAssets = ['home_phone_image', 'feature_phone_image', 'download_phone_image', 'about_logo_image'];
        $nameAssets = ['header-mobile.png', 'feature-image.png', 'download-image.png', 'about-logo.png'];
        for ($i = 0; $i < count($postAssets); $i++) {
            if ($request->hasFile($postAssets[$i])) {
                $imageFile = $request->file($postAssets[$i]);
                $imageName = $nameAssets[$i];
                $destinationPath = public_path('/appy/images');
                $imageFile->move($destinationPath, $imageName);
            }
        }

        foreach ($request->all() as $key => $value) {
            if (isset($value) && !in_array($key, $postAssets)) {
                $findWebContent = WebContent::where('key', $key)->first();
                $findWebContent->value = $value;
                $findWebContent->save();
            }
        }

        return $this->sendResponse('Content saved successfully', $request->toArray());
    }

    public function getContentArticle()
    {
        $article = WebArticle::whereNotIn('tag', ['promo'])->get();

        return response()->json($article);
    }

    public function createContentArticle(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tag'       => 'required',
            'title'     => 'required',
            'content'   => 'required',
        ]);

        if ($validator->fails() || !$request->hasFile('image')) {
            return $this->sendError('The given data was invalid');
        }

        $imageFile = $request->file('image');
        $imageName = time() . '.' . $imageFile->getClientOriginalExtension();
        $destinationPath = public_path('/appy/images/article');
        $imageFile->move($destinationPath, $imageName);

        $createArticle = new WebArticle;
        $createArticle->tag = $request->tag;
        $createArticle->title = $request->title;
        $createArticle->content = $request->content;
        $createArticle->image = '/appy/images/article/' . $imageName;
        if ($createArticle->save()) {
            return $this->sendResponse('Article created succesfully', $createArticle);
        }

        return $this->sendError('Article create failed');
    }

    public function updateContentArticle($article_id, Request $request)
    {
        $findArticle = WebArticle::find($article_id);

        isset($request->tag) ? $findArticle->tag = $request->tag : null;
        isset($request->title) ? $findArticle->title = $request->title : null;
        isset($request->content) ? $findArticle->content = $request->content : null;

        if ($request->hasFile('image')) {
            $imageFile = $request->file('image');
            $imageName = time() . '.' . $imageFile->getClientOriginalExtension();
            $destinationPath = public_path('/appy/images/article');
            $imageFile->move($destinationPath, $imageName);
            $findArticle->image = '/appy/images/article/' . $imageName;
        }

        if ($findArticle->save()) {
            return $this->sendResponse('Article updated successfully', $findArticle);
        }

        return $this->sendError('Article update failed');
    }

    public function deleteContentArticle($article_id)
    {
        $findArticle = WebArticle::find($article_id);

        if ($findArticle == null) {
            return $this->sendError('Article not found');
        }

        $deleteArticle = WebArticle::destroy($article_id);

        if ($deleteArticle) {
            return $this->sendResponse('Article deleted succesfully', '');
        }

        return $this->sendError('Article delete failed');
    }

    public function getContentPromo()
    {
        $article = WebArticle::where('tag', 'promo')->get();

        return response()->json($article);
    }
}
