<?php

namespace App\Http\Controllers\Api;

use App\WebAdmin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'    => 'required',
            'password'    => 'required',
        ]);

        if ($validator->fails()) {
            return $this->sendError('The given data was invalid');
        }

        $findUser = WebAdmin::where('username', $request->username)->first();
        if ($findUser == null) {
            return $this->sendError('Wrong username or password');
        }

        if (md5($request->password) !== $findUser->password) {
            return $this->sendError('Wrong username or password');
        }

        return response()->json([
            'access_token'  => '123',
            'level'         => 'w',
        ]);
    }
}
