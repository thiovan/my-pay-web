<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="title" content="{{ $value->meta_title }}">
    <meta name="author" content="{{ $value->meta_author }}">
    <meta name="description" content="{{ $value->meta_description }}">
    <meta name="keywords" content="{{ $value->meta_keywords }}">
    <meta name="robots" content="{{ $value->meta_robots }}">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>{{ $value->title }}</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{ URL::asset('appy/images/favicon.png') }}">
    <link rel="shortcut icon" type="image/ico" href="{{ URL::asset('appy/images/favicon.ico') }}" />
    <link rel="icon" href="{{ URL::asset('appy/images/favicon.png') }}" sizes="16x16" type="image/png">
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="{{ URL::asset('appy/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appy/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appy/css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appy/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appy/css/animate.css') }}">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="{{ URL::asset('appy/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appy/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appy/css/responsive.css') }}">
    <script src="{{ URL::asset('appy/js/vendor/modernizr-2.8.3.min.js') }}"></script>
    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-spy="scroll" data-target=".mainmenu-area">
    <!-- Preloader-content -->
    <div class="preloader d-flex justify-content-center align-middle">
        <span><img class="img-fluid" style="height: 3.5rem; margin-bottom: 0.5rem; margin-left: 0.5rem;" src="{{ URL::asset('appy/images/preloader-logo.png') }}" alt="Logo"></span>
    </div>
    <!-- MainMenu-Area -->
    <nav class="mainmenu-area" data-spy="affix" data-offset-top="200">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary_menu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img class="img-fluid" src="{{ URL::asset('appy/images/logo.png') }}" alt="Logo"></a>
            </div>
            <div class="collapse navbar-collapse" id="primary_menu">
                <ul class="nav navbar-nav mainmenu pull-left">
                    @for ($i = 0; $i < count($value->menu); $i++)
                        @if ($i == 0)
                        <li class="active"><a href="{{ $value->menu[$i]->href }}">{{ $value->menu[$i]->name }}</a></li>
                        @else
                        <li><a href="{{ $value->menu[$i]->href }}">{{ $value->menu[$i]->name }}</a></li>
                        @endif
                        @endfor
                </ul>
                <div class="right-button hidden-xs">
                    @for ($i = 0; $i < count($value->menu_button); $i++)
                        @if ($i == 0)
                        <a href="{{ $value->menu_button[$i]->href }}">{{ $value->menu_button[$i]->name }}</a>
                        @else
                        <a style="margin-left: 2vh" href="{{ $value->menu_button[$i]->href }}">{{ $value->menu_button[$i]->name }}</a>
                        @endif
                        @endfor
                </div>
            </div>
        </div>
    </nav>
    <!-- MainMenu-Area-End -->
    <!-- Home-Area -->
    <header class="home-area overlay" id="home_page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 hidden-sm col-md-5">
                    <figure class="mobile-image wow fadeInUp" data-wow-delay="0.2s">
                        <img style="margin-bottom: 0.7rem;" src="{{ URL::asset('appy/images/header-mobile.png') }}" alt="">
                    </figure>
                </div>
                <div class="col-xs-12 col-md-7">
                    <div class="space-20 hidden-xs"></div>
                    <h2 class="wow fadeInUp" data-wow-delay="0.4s">{{ $value->home_title }}</h2>
                    <div class="space-20"></div>
                    <div class="desc wow fadeInUp" data-wow-delay="0.6s">
                        <p>{{ $value->home_sub_title }}</p>
                    </div>
                    <div class="space-20"></div>
                    <a class="wow fadeInUp" data-wow-delay="0.8s" href="{{ $value->download_android }}"><img class="bttn-download img-fluid" src="{{ URL::asset('appy/images/play-store-button.png') }}" alt="Play Store Icon"></a>
                </div>
            </div>
        </div>
    </header>
    <!-- Home-Area-End -->
    <!-- About-Area -->
    <section class="section-padding" id="about_page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-10 col-md-offset-1">
                    <div class="page-title text-center">
                        <img src="{{ URL::asset('appy/images/about-logo.png') }}" alt="About Logo">
                        <div class="space-20"></div>
                        <h5 class="title">Tentang My-Pay</h5>
                        <div class="space-30"></div>
                        <h3 class="blue-color">{{ $value->about_title }}</h3>
                        <div class="space-20"></div>
                        <p>{{ $value->about_desc }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About-Area-End -->
    <!-- Feature-Area -->
    <section class="feature-area section-padding-top" id="features_page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                    <div class="page-title text-center">
                        <h5 class="title">Layanan</h5>
                        <div class="space-10"></div>
                        <h3>Layanan Yang Kami Berikan</h3>
                        <div class="space-30"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="space-50"></div>
                    @for ($i = 0; $i < count($value->service); $i++)
                        @if ($i % 2 == 0)
                        @if ($i == 0)
                        <div class="service-box wow fadeInUp" data-wow-delay="0.2s">
                            <div class="box-icon">
                                <img class="img-responsive" style="padding: 1vh;" src="{{ URL::asset($value->service[$i]->icon) }}" alt="My-Charity">
                            </div>
                            <h4>{{ $value->service[$i]->title }}</h4>
                            <p>{{ $value->service[$i]->desc }}</p>
                            <a href="{{ $value->service[$i]->href }}" class="wow fadeInUp" data-wow-delay="0.8s" style="color: white; font-weight: 800; visibility: visible; animation-delay: 0.8s; animation-name: fadeInUp;">Read
                                More >></a>
                        </div>
                        <div class="space-60"></div>
                        @else
                        <div class="service-box wow fadeInUp" data-wow-delay="0.4s">
                            <div class="box-icon">
                                <img class="img-responsive" style="padding: 1vh;" src="{{ URL::asset($value->service[$i]->icon) }}" alt="My-Store">
                            </div>
                            <h4>{{ $value->service[$i]->title }}</h4>
                            <p>{{ $value->service[$i]->desc }}</p>
                            <a href="{{ $value->service[$i]->href }}" class="wow fadeInUp" data-wow-delay="0.8s" style="color: white; font-weight: 800; visibility: visible; animation-delay: 0.8s; animation-name: fadeInUp;">Read
                                More >></a>
                        </div>
                        <div class="hidden-xs space-60"></div>
                        @endif
                        @endif
                        @endfor
                </div>

                <div class="hidden-xs hidden-sm col-md-4">
                    <figure class="mobile-image">
                        <img src="{{ URL::asset('appy/images/feature-image.png') }}" alt="Feature Photo">
                    </figure>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-4">
                    <div class="space-50"></div>
                    @for ($i = 0; $i < count($value->service); $i++)
                        @if ($i % 2 != 0)
                        @if ($i == 0)

                        <div class="service-box wow fadeInUp" data-wow-delay="0.2s">
                            <div class="box-icon">
                                <img class="img-responsive" style="padding: 1vh;" src="{{ URL::asset($value->service[$i]->icon) }}" alt="My-Charity">
                            </div>
                            <h4>{{ $value->service[$i]->title }}</h4>
                            <p>{{ $value->service[$i]->desc }}</p>
                            <a href="{{ $value->service[$i]->href }}" class="wow fadeInUp" data-wow-delay="0.8s" style="color: white; font-weight: 800; visibility: visible; animation-delay: 0.8s; animation-name: fadeInUp;">Read
                                More >></a>
                        </div>
                        <div class="space-60"></div>
                        @else
                        <div class="service-box wow fadeInUp" data-wow-delay="0.4s">
                            <div class="box-icon">
                                <img class="img-responsive" style="padding: 1vh;" src="{{ URL::asset($value->service[$i]->icon) }}" alt="My-Store">
                            </div>
                            <h4>{{ $value->service[$i]->title }}</h4>
                            <p>{{ $value->service[$i]->desc }}</p>
                            <a href="{{ $value->service[$i]->href }}" class="wow fadeInUp" data-wow-delay="0.8s" style="color: white; font-weight: 800; visibility: visible; animation-delay: 0.8s; animation-name: fadeInUp;">Read
                                More >></a>
                        </div>
                        <div class="space-60"></div>
                        @endif
                        @endif
                        @endfor
                </div>
            </div>
        </div>
    </section>
    <!-- Feature-Area-End -->
    <!-- Download-Area -->
    <div class="download-area overlay" id="download_page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 hidden-sm">
                    <figure class="mobile-image">
                        <img src="{{ URL::asset('appy/images/download-image.png') }}" alt="">
                    </figure>
                </div>
                <div class="col-xs-12 col-md-6 section-padding">
                    <h3 class="white-color">{{ $value->download_title }}</h3>
                    <div class="space-20"></div>
                    <p>{{ $value->download_desc }}</p>
                    <div class="space-60"></div>
                    <a href="{{ $value->download_android }}"><img class="bttn-download img-fluid" src="{{ URL::asset('appy/images/play-store-button.png') }}" alt="Play Store Icon"></a>
                </div>
            </div>
        </div>
    </div>
    <!-- Download-Area-End -->
    <!--Promo-Area -->
    <section class="section-padding price-area" id="promo_page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title text-center">
                        <h5 class="title">Promo</h5>
                        <h3 class="dark-color">Promo My-Pay</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="team-slide">

                        @foreach ($promos as $promo)
                        <div style="padding: 1em; border-radius: 1em" class="team-box">
                            <div>
                                <img style="border-radius: 0.5em;" src="{{ URL::asset($promo->image) }}" alt="" />
                                <div class="space-20"></div>
                                <p>{{ $promo->title }}</p>
                                <div class="space-20"></div>
                                <a href="{{ url('/article/' . $promo->id) }}" class="bttn-default wow fadeInUp" data-wow-delay="0.8s" style="visibility: visible; animation-delay: 0.8s; animation-name: fadeInUp;">Selengkapnya
                                    ></a>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Promo-Area-End -->
    <!-- News Area -->
    <section class="section-padding" id="news_page">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title text-center">
                        <h5 class="title">Artikel</h5>
                        <h3 class="dark-color">My-Pay Artikel</h3>
                        <div class="space-20"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="news-slide">

                        @foreach ($articles as $article)
                        <div class="col-xs-12">
                            <article class="post-single sticky">
                                <figure class="post-media">
                                    <img src="{{ URL::asset($article->image) }}" alt="">
                                </figure>
                                <div class="post-body">
                                    <div class="post-meta">
                                        <div class="post-tags"><a href="#">{{ $article->tag }}</a></div>
                                        <div class="post-date">{{ date_format($article->created_at, 'd.m.Y') }}</div>
                                    </div>
                                    <h4 class="dark-color"><a href="{{ url('/article/' . $article->id) }}">{{ $article->title }}</a></h4>
                                    <p>{!! substr(strip_tags($article->content), 0, 200) . '...' !!}</p>
                                    <a href="{{ url('/article/' . $article->id) }}" class="read-more">View Article</a>
                                </div>
                            </article>
                            <div class="space-20"></div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- News Area End -->
    <!--Questions-Area -->
    <section id="faq_page" class="questions-area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title text-center">
                        <h5 class="title">FAQ</h5>
                        <h3 class="dark-color">Frequently Asked Questions</h3>
                        <div class="space-60"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="toggole-boxs">

                        @for ($i = 0; $i < count($value->faq); $i++)
                            @if ($i % 2 == 0)
                            <h3>{{ $value->faq[$i]->question }}</h3>
                            <div>
                                <p>{{ $value->faq[$i]->answer }}</p>
                            </div>
                            @endif
                            @endfor

                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="space-20 hidden visible-xs"></div>
                    <div class="toggole-boxs">

                        @for ($i = 0; $i < count($value->faq); $i++)
                            @if ($i % 2 != 0)
                            <h3>{{ $value->faq[$i]->question }}</h3>
                            <div>
                                <p>{{ $value->faq[$i]->answer }}</p>
                            </div>
                            @endif
                            @endfor

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Questions-Area-End -->
    <!-- Footer-Area -->
    <footer class="footer-area" id="contact_page">
        <div class="cs-number-box" style="background: linear-gradient(45deg, rgba(0, 64, 166, 1) 0%, rgba(101, 150, 255, 1) 100%); text-align: center; transform: translate(-50%, -50%); margin-top: 0px; padding: 1em; border-radius: 0.5em; display: inline-block; top: 0; position: relative;">
            <h3 class="dark-color" style="color: white; font-weight: bold; visibility: visible;">{{ $value->cs_phone }}</h3>
            <h5 class="dark-color" style="line-height: 0; color: white; font-weight: bold; visibility: visible;">
                24 Jam | Senin - Minggu</h5>
        </div>
        <div style="padding-top: 0;" class="section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="page-title text-center">
                            <h5 class="title">Contact US</h5>
                            <h3 class="dark-color">Find Us By Below Details</h3>
                            <div class="space-60"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    @foreach ($value->contact as $contact)
                        <div class="col-xs-12 col-sm-4">
                            <div class="footer-box">
                                <div class="box-icon">
                                    <span class="lnr {{ $contact->icon_name }}"></span>
                                </div>
                                <p>
                                    {!! !empty($contact->line_1) ? $contact->line_1 : '<br>' !!}
                                    <br />
                                    {!! !empty($contact->line_2) ? $contact->line_2 : '<br>' !!}
                                </p>
                            </div>
                            <div class="space-30 hidden visible-xs"></div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!-- Footer-Bootom -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-5">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        <span>Copyright &copy;
                            <script>
                                document.write(new Date().getFullYear());
                            </script> My-Pay. All rights reserved
                        </span>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        <div class="space-30 hidden visible-xs"></div>
                    </div>
                    <div class="col-xs-12 col-md-7">
                        <div class="footer-menu">
                            <ul>
                                @foreach ($value->menu as $menu)
                                    <li><a href="{{ $menu->href }}">{{ $menu->name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer-Bootom-End -->
    </footer>
    <!-- Footer-Area-End -->
    <!--Vendor-JS-->
    <script src="{{ URL::asset('appy/js/vendor/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ URL::asset('appy/js/vendor/jquery-ui.js') }}"></script>
    <script src="{{ URL::asset('appy/js/vendor/bootstrap.min.js') }}"></script>
    <!--Plugin-JS-->
    <script src="{{ URL::asset('appy/js/owl.carousel.min.js') }}"></script>
    <script src="{{ URL::asset('appy/js/contact-form.js') }}"></script>
    <script src="{{ URL::asset('appy/js/ajaxchimp.js') }}"></script>
    <script src="{{ URL::asset('appy/js/scrollUp.min.js') }}"></script>
    <script src="{{ URL::asset('appy/js/magnific-popup.min.js') }}"></script>
    <script src="{{ URL::asset('appy/js/wow.min.js') }}"></script>
    <!--Main-active-JS-->
    <script src="{{ URL::asset('appy/js/main.js') }}"></script>
</body>

</html>
