<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MyPay - Privacy Policy</title>
    <link rel="stylesheet" href="{{ URL::asset('appy/css/bootstrap.min.css') }}">
    <style>
        @media (min-width: 768px) {
            #features .card {
                min-height: 288px;
            }
        }
        @media (min-width: 992px) {
            #features .card {
                min-height: 312px;
            }
        }
        .register-address {
            background-color: #003C7F;
        }
        @media (max-width: 767.98px) {
            .register-address {
                margin-left: 1rem;
                margin-right: 1rem;
                border-top-right-radius: 0!important;
                border-bottom-left-radius: .25rem!important;
            }
        }
        .form-control {
            border-width: 0;
            border-bottom-width: 1px;
            border-radius: 0;
            padding-left: 0;
            padding-right: 0;
        }
        .form-control:focus {
            box-shadow: none;
            border-bottom-width: 2px;
        }
        #footer {
            background-color: #f0f3f7;
        }
    </style>
</head>
<body class="bg-primary">
    <nav class="navbar fixed-top navbar-dark">
        <div class="container">
            <a class="navbar-brand" href="/">

            </a>
        </div>
    </nav>

    <section id="register" class="bg-light py-5">
        <div class="container">
            <div class="text-left pb-5 bg-light">
                <img src="http://dashboard.my-pay.id/dist/img/logo.png" width="150">
                <h3>Syarat & Ketentuan</h3>
            </div>
        </div>

        <div class="container">
            <div class="card border-0 shadow-sm">
                <div class="row">
                    <div class="col-md-8 p-5 text-justify">
<p>Effective date: August 22, 2019</p>

<p>Halaman ini memberi tahu Anda tentang kebijakan kami mengenai pengumpulan, penggunaan, dan pengungkapan data pribadi saat Anda menggunakan Layanan kami dan pilihan yang telah Anda kaitkan dengan data itu. Kebijakan Privasi kami untuk MyPay</p>

<p>Kami menggunakan data Anda untuk menyediakan dan meningkatkan Layanan. Dengan menggunakan Layanan, Anda menyetujui pengumpulan dan penggunaan informasi sesuai dengan kebijakan ini. Kecuali ditentukan lain dalam Kebijakan Privasi ini, istilah yang digunakan dalam Kebijakan Privasi ini memiliki arti yang sama seperti dalam Syarat dan Ketentuan kami.
</p>


<h2>Pengumpulan Dan Penggunaan Informasi</h2>

<p>Kami mengumpulkan beberapa jenis informasi yang berbeda untuk berbagai keperluan untuk menyediakan dan meningkatkan Layanan kami kepada Anda.</p>

<h3>Jenis Data yang Dikumpulkan</h3>

<h4>Data pribadi</h4>

<p>Saat menggunakan Layanan kami, kami mungkin meminta Anda untuk memberikan kami informasi pengenal pribadi tertentu yang dapat digunakan untuk menghubungi atau mengidentifikasi Anda ("Data Pribadi"). Informasi yang dapat diidentifikasi secara pribadi dapat mencakup, tetapi tidak terbatas pada:</p>
<p>Alamat email,
Nama depan dan nama belakang,
Nomor telepon,
Alamat, Negara Bagian, Provinsi, ZIP / Kode pos, Kota,
Cookie dan Data Penggunaan</p>

<h4>Data Penggunaan</h4>

<p>Ketika Anda mengakses Layanan dengan perangkat seluler, kami dapat mengumpulkan informasi tertentu secara otomatis, termasuk, tetapi tidak terbatas pada, jenis perangkat seluler yang Anda gunakan, ID unik perangkat seluler Anda, alamat IP perangkat seluler Anda, sistem operasi seluler Anda , jenis browser Internet seluler yang Anda gunakan, pengidentifikasi perangkat unik, dan data diagnostik lainnya ("Data Penggunaan").</p>

<h4>Pelacakan & Data Cookie</h4>
<p>Kami menggunakan cookie dan teknologi pelacakan serupa untuk melacak aktivitas di Layanan kami dan menyimpan informasi tertentu.</p>
<p>Cookie adalah file dengan sedikit data yang dapat menyertakan pengidentifikasi unik anonim. Cookie dikirim ke browser Anda dari situs web dan disimpan di perangkat Anda. Teknologi pelacakan yang juga digunakan adalah suar, tag, dan skrip untuk mengumpulkan dan melacak informasi dan untuk meningkatkan dan menganalisis Layanan kami.</p>
<p>Anda dapat menginstruksikan browser Anda untuk menolak semua cookie atau untuk menunjukkan kapan cookie dikirim. Namun, jika Anda tidak menerima cookie, Anda mungkin tidak dapat menggunakan sebagian Layanan kami.</p>
<h2>Penggunaan Data</h2>

<p>MyPay menggunakan data yang dikumpulkan untuk berbagai keperluan:</p>
<ul>
    <li>Untuk menyediakan dan memelihara Layanan
</li>
    <li>Untuk memberitahukan Anda tentang perubahan didalam pelayanan kami
</li>
    <li>Untuk memungkinkan Anda berpartisipasi dalam fitur interaktif Layanan kami ketika Anda memilih untuk melakukannya
</li>
    <li>Untuk memberikan layanan dan dukungan pelanggan
</li>
    <li>Untuk memberikan analisis atau informasi berharga sehingga kami dapat meningkatkan Layanan
</li>
    <li>Untuk memantau penggunaan Layanan
</li>
    <li>Untuk mendeteksi, mencegah dan mengatasi masalah teknis
</li>
</ul>

<h2>Transfer Data</h2>
<p>Informasi Anda, termasuk Data Pribadi, dapat ditransfer ke - dan dipelihara di - komputer yang berlokasi di luar negara bagian, provinsi, negara atau yurisdiksi pemerintah lainnya di mana undang-undang perlindungan data mungkin berbeda dari yang ada di yurisdiksi Anda.
</p>
<p>Persetujuan Anda untuk Kebijakan Privasi ini diikuti dengan pengiriman informasi tersebut merupakan persetujuan Anda untuk transfer tersebut.
</p>
<p>MyPay akan mengambil semua langkah yang wajar diperlukan untuk memastikan bahwa data Anda diperlakukan dengan aman dan sesuai dengan Kebijakan Privasi ini dan tidak ada transfer Data Pribadi Anda yang akan terjadi pada suatu organisasi atau negara kecuali jika ada kontrol yang memadai di tempat termasuk keamanan Anda data dan informasi pribadi lainnya.
</p>

<h2>Keterbukaan Data</h2>

<h3>Persyaratan resmi
</h3>
<p>MyPay dapat mengungkapkan Data Pribadi Anda dengan itikad baik bahwa tindakan tersebut diperlukan untuk:</p>
<ul>
    <li>Untuk mematuhi kewajiban hukum
</li>
    <li>Untuk melindungi dan mempertahankan hak atau properti MyPay
</li>
    <li>Untuk mencegah atau menyelidiki kesalahan yang mungkin terjadi sehubungan dengan Layanan
</li>
    <li>Untuk melindungi keamanan pribadi pengguna Layanan atau publik
</li>
    <li>Untuk melindungi terhadap tanggung jawab hukum
</li>
</ul>

<h2>Keamanan Data
</h2>
<p>Keamanan data Anda penting bagi kami, tetapi ingat bahwa tidak ada metode transmisi melalui Internet, atau metode penyimpanan elektronik yang 100% aman. Meskipun kami berusaha untuk menggunakan cara yang dapat diterima secara komersial untuk melindungi Data Pribadi Anda, kami tidak dapat menjamin keamanan mutlaknya.
</p>

<h2>Penyedia jasa
</h2>
<p>Kami dapat mempekerjakan perusahaan pihak ketiga dan individu untuk memfasilitasi Layanan kami ("Penyedia Layanan"), untuk memberikan Layanan atas nama kami, untuk melakukan layanan terkait Layanan atau untuk membantu kami dalam menganalisis bagaimana Layanan kami digunakan.
</p>
<p>Pihak ketiga ini memiliki akses ke Data Pribadi Anda hanya untuk melakukan tugas-tugas ini atas nama kami dan berkewajiban untuk tidak mengungkapkan atau menggunakannya untuk tujuan lain apa pun.
</p>

<h2>Keamanan Pembayaran</h2>
<p>Data transaksi pembelian Anda hanya disimpan sepanjang diperlukan untuk menyelesaikan transaksi di layanan kami sesuai dengan ketentuan yang berlaku. Untuk mengetahui lebih lanjut mengenai keamanan pembayaran dan sistem pembayaran di layanan kami, silahkan Anda membaca Syarat & Ketentuan Layanan MyPay.
</p>

<h2>Tautan ke Situs Lain
</h2>
<p>Layanan kami dapat berisi tautan ke situs lain yang tidak dioperasikan oleh kami. Jika Anda mengklik tautan pihak ketiga, Anda akan diarahkan ke situs pihak ketiga itu. Kami sangat menyarankan Anda untuk meninjau Kebijakan Privasi setiap situs yang Anda kunjungi.
</p>
<p>Kami tidak memiliki kendali atas dan tidak bertanggung jawab atas konten, kebijakan privasi, atau praktik situs atau layanan pihak ketiga mana pun.
</p>


<h2>Privasi anak-anak
</h2>
<p>Layanan kami tidak membahas siapa pun yang berusia di bawah 18 ("Anak-anak").</p>
<p>Kami tidak secara sadar mengumpulkan informasi yang dapat diidentifikasi secara pribadi dari siapa pun yang berusia di bawah 18 tahun. Jika Anda adalah orang tua atau wali dan Anda mengetahui bahwa Anak-anak Anda telah memberikan kepada kami Data Pribadi, silakan hubungi kami. Jika kami mengetahui bahwa kami telah mengumpulkan Data Pribadi dari anak-anak tanpa verifikasi izin orang tua, kami mengambil langkah-langkah untuk menghapus informasi itu dari server kami.
</p>


<h2>Perubahan Terhadap Kebijakan Privasi Ini
</h2>
<p>Kami dapat memperbarui Kebijakan Privasi kami dari waktu ke waktu. Kami akan memberi tahu Anda tentang segala perubahan dengan memposting Kebijakan Privasi baru di halaman ini.
</p>
<p>Kami akan memberi tahu Anda melalui email dan / atau pemberitahuan penting pada Layanan kami, sebelum perubahan menjadi efektif dan memperbarui "tanggal efektif" di bagian atas Kebijakan Privasi ini.
</p>
<p>Anda disarankan untuk meninjau Kebijakan Privasi ini secara berkala untuk setiap perubahan. Perubahan pada Kebijakan Privasi ini efektif ketika diposkan pada halaman ini.
</p>


<h2>Hubungi kami
</h2>
<p>Jika Anda memiliki pertanyaan tentang Kebijakan Privasi ini, silakan hubungi kami:
</p>
<ul>
        <li>By email: admin@my-pay.id</li>
            <li>Dengan mengunjungi halaman ini di situs web kami: http://my-pay.id</li>

        </ul>
                    </div>
                    <div class="register-address col-md-4 p-5 rounded-right">
                        <h5 class="mb-4 text-white">Contact Us</h5>

                        <h6 class="text-warning">Where to find us</h6>
                        <p class="text-light">IT Support-Operation Jawa Tengah and DIY Gedung Telkomsel lt. 5 <br>Jl. Pahlawan No.10, Pleburan, Semarang Selatan, Kota Semarang, Jawa Tengah. 50241</p>

                        <h6 class="text-warning mt-5">Email us</h6>
                        <p class="text-light">it.jatengdiy@gmail.com</p>

                        <h6 class="text-warning mt-5">Call us</h6>
                        <p class="text-light mb-0">Phone: (+62) 811 2782 811</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer id="footer" class="text-center py-5">
        <p class="text-secondary mb-0">Copyright My-Pay &copy; 2018</p>
    </footer>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="{{ URL::asset('appy/js/vendor/bootstrap.min.js') }}"></script>
</body>
</html>
