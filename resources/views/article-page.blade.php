<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta name="author" content="My-Pay">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Title -->
    <title>{{ $value->title }}</title>
    <!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="{{ URL::asset('appy/images/apple-touch-icon.png') }}">
    <link rel="shortcut icon" type="image/ico" href="{{ URL::asset('appy/images/favicon.ico') }}" />
    <!-- Plugin-CSS -->
    <link rel="stylesheet" href="{{ URL::asset('appy/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appy/css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appy/css/linearicons.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appy/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appy/css/animate.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appy/css/quill.snow.css') }}">
    <!-- Main-Stylesheets -->
    <link rel="stylesheet" href="{{ URL::asset('appy/css/normalize.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appy/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appy/css/responsive.css') }}">
    <script src="{{ URL::asset('appy/js/vendor/modernizr-2.8.3.min.js') }}"></script>
    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-spy="scroll" data-target=".mainmenu-area">
    <!-- Preloader-content -->
    <div class="preloader">
        <span><img height="25px" width="25px" src="{{ URL::asset('appy/images/preloader-logo.png') }}" alt="Logo"></span>
    </div>
    <!-- MainMenu-Area -->
    <nav class="mainmenu-area" data-spy="affix" data-offset-top="200">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary_menu">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><img src="{{ URL::asset('appy/images/logo.png') }}" alt="Logo"></a>
            </div>
            <div class="collapse navbar-collapse" id="primary_menu">
                <ul class="nav navbar-nav mainmenu pull-left">
                    @for ($i = 0; $i < count($value->menu); $i++)
                        @if ($i == 5)
                        <li class="active"><a href="{{ URL::to('/') . $value->menu[$i]->href }}">{{ $value->menu[$i]->name }}</a></li>
                        @else
                        <li><a href="{{ URL::to('/') . $value->menu[$i]->href }}">{{ $value->menu[$i]->name }}</a></li>
                        @endif
                        @endfor
                </ul>
                <div class="right-button hidden-xs">
                    @for ($i = 0; $i < count($value->menu_button); $i++)
                        @if ($i == 0)
                        <a href="{{ $value->menu_button[$i]->href }}">{{ $value->menu_button[$i]->name }}</a>
                        @else
                        <a style="margin-left: 2vh" href="{{ $value->menu_button[$i]->href }}">{{ $value->menu_button[$i]->name }}</a>
                        @endif
                        @endfor
                </div>
            </div>
        </div>
    </nav>
    <!-- MainMenu-Area-End -->
    <header style="padding: 0px 0 80px;" class="site-header">
        <div class="container">
            <div class="row">
            </div>
        </div>
    </header>

    <div style="padding-top: 20px;" class="section-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                        <div class="post-body">
                            <ul class="breadcrumb">
                                <li><a href="#">{{ $article->tag }}</a></li>
                                <li>{{ $article->created_at }}</li>
                            </ul>
                            <h2 class="dark-color">{{ $article->title }}</h2>
                            {!! $article->content !!}
                        </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer-Area -->
    <footer class="footer-area" id="contact_page">
        <!-- Footer-Bootom -->
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-5">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        <span>Copyright &copy;
                            <script>
                                document.write(new Date().getFullYear());
                            </script> My-Pay. All rights reserved
                        </span>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        <div class="space-30 hidden visible-xs"></div>
                    </div>
                    <div class="col-xs-12 col-md-7">
                        <div class="footer-menu">
                            <ul>
                                <li><a href="#about_page">Tentang Kami</a></li>
                                <li><a href="#features_page">Layanan</a></li>
                                <li><a href="#download_page">Download</a></li>
                                <li><a href="#promo_page">Promo</a></li>
                                <li><a href="#news_page">Artikel</a></li>
                                <li><a href="#faq_page">FAQ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer-Bootom-End -->
    </footer>
    <!-- Footer-Area-End -->
    <!--Vendor-JS-->
    <script src="{{ URL::asset('appy/js/vendor/jquery-1.12.4.min.js') }}"></script>
    <script src="{{ URL::asset('appy/js/vendor/jquery-ui.js') }}"></script>
    <script src="{{ URL::asset('appy/js/vendor/bootstrap.min.js') }}"></script>
    <!--Plugin-JS-->
    <script src="{{ URL::asset('appy/js/owl.carousel.min.js') }}"></script>
    <script src="{{ URL::asset('appy/js/contact-form.js') }}"></script>
    <script src="{{ URL::asset('appy/js/ajaxchimp.js') }}"></script>
    <script src="{{ URL::asset('appy/js/scrollUp.min.js') }}"></script>
    <script src="{{ URL::asset('appy/js/magnific-popup.min.js') }}"></script>
    <script src="{{ URL::asset('appy/js/wow.min.js') }}"></script>
    <!--Main-active-JS-->
    <script src="{{ URL::asset('appy/js/main.js') }}"></script>
</body>

</html>
