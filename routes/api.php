<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', 'Api\AuthController@login');
});
Route::group(['prefix' => 'admin'], function () {
    Route::get('/content/landing', 'Api\WebContentController@getContentLanding');
    Route::post('/content/landing', 'Api\WebContentController@postContentLanding');
    Route::get('/content/article', 'Api\WebContentController@getContentArticle');
    Route::post('/content/article', 'Api\WebContentController@createContentArticle');
    Route::post('/content/article/{article_id}', 'Api\WebContentController@updateContentArticle')->where('article_id', '[0-9]+');
    Route::delete('/content/article/{article_id}', 'Api\WebContentController@deleteContentArticle')->where('article_id', '[0-9]+');
    Route::get('/content/promo', 'Api\WebContentController@getContentPromo');
});
