<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebController@landingPage');
Route::get('/article/{article_id}', 'WebController@articlePage');
Route::get('/privacy-policy', 'WebController@privacyPolicy');
